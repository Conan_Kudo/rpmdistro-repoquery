# rpmdistro-repoquery

This tool utilizes [DNF](http://dnf.baseurl.org) to let you query across several RPM-based Linux distributions.

Currently, the tool supports the following distributions:

* Fedora
* Mageia
* openSUSE Leap
* openSUSE Tumbleweed
* CentOS (with EPEL)
* CentOS Stream (with EPEL)
* Red Hat Enterprise Linux Universal Base Image (RHEL UBI)
* SUSE Linux Enterprise Base Container Image (SLE BCI)

# How to use it

Ensure that the `dnf` and `distribution-gpg-keys` packages are installed on your system.

Both are available in the following distributions:

* Fedora
* Mageia (since Mageia 6)
* openSUSE Leap (since 15.0)
* openSUSE Tumbleweed (since April 2018)
* CentOS (since 7.6) with EPEL installed and enabled

```bash
$ ./rpmdistro-repoquery ./distros/<distro>.conf <releasever> [<repoquery options> ...] <pkg-or-file-path>

```

## openSUSE specific notes

For a special case, openSUSE Tumbleweed would work as the following with repoquery:

```bash

$ ./rpmdistro-repoquery ./distros/opensuse-tumbleweed.conf 0 [<repoquery options> ...] <pkg-or-file-path>

```

This is because openSUSE Tumbleweed doesn't have a defined release ver, so you can put whatever value you want in the second position.

However, if you want to query various released snapshots for comparisons, you can:

```bash

$ ./rpmdistro-repoquery ./distros/opensuse-tumbleweed-snapshot.conf 20190423 [<repoquery options> ...] <pkg-or-file-path>

```

A [list of available Tumbleweed snapshots](http://download.opensuse.org/history/list) to query is published by openSUSE.

## Mageia specific notes

For Mageia, the configuration needs to be generated first:

```bash
$ ./rpmdistro-gendnfconf -d mageia -r 7 -o distro.conf
$ ./rpmdistro-repoquery ./distro.conf 7 [<repoquery options> ...] <pkg-or-file-path>

```

## SLE BCI specific notes

For SLE BCI, the configuration needs to be generated first:

```bash
$ ./rpmdistro-gendnfconf -d sle-bci -r 15.3 -o distro.conf
$ ./rpmdistro-repoquery ./distro.conf 15.3 [<repoquery options> ...] <pkg-or-file-path>
```
